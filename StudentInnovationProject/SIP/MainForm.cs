﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using SlimDX.DirectInput;

namespace SIP
{
    public partial class MainForm : Form
    {
        List<DeviceInstance> dinputList = new List<DeviceInstance>();
        DirectInput dinput = new DirectInput();
        Joystick[] gamepad;
        JoystickState state;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            dinputList.AddRange(dinput.GetDevices(DeviceClass.GameController, DeviceEnumerationFlags.AttachedOnly));
            Debug.WriteLine(dinputList.Count);
            gamepad = new Joystick[dinputList.Count];
            for (int index = 0; index < dinputList.Count; index++)
            {
                gamepad[index] = new SlimDX.DirectInput.Joystick(dinput, dinputList[0].InstanceGuid);
                gamepad[index].SetCooperativeLevel(this, CooperativeLevel.Nonexclusive | CooperativeLevel.Background);
                gamepad[index].Acquire();
                Debug.WriteLine(gamepad[index].Acquire().Name);
                foreach (DeviceObjectInstance deviceObject in gamepad[index].GetObjects())
                {
                    if ((deviceObject.ObjectType & ObjectDeviceType.Axis) != 0)
                        gamepad[index].GetObjectPropertiesById((int)deviceObject.ObjectType).SetRange(-1000, 1000);
                }

                Debug.WriteLine(dinputList[index].ProductName + " " + gamepad[index].Properties.InstanceName);
            }
            mainTimer.Start();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void mainTimer_Tick_1(object sender, EventArgs e)
        {
            for (int gamepadIndex = 0; gamepadIndex < gamepad.Length; gamepadIndex++)
            {
                if (gamepad[gamepadIndex].Acquire().IsFailure)
                {
                    Debug.WriteLine("I");
                    return;
                }

                if (gamepad[gamepadIndex].Poll().IsFailure)
                {
                    Debug.WriteLine("Hate");
                    return;
                }

                if (SlimDX.Result.Last.IsFailure)
                {
                    Debug.WriteLine("This");
                    return;
                }

                state = gamepad[gamepadIndex].GetCurrentState();
                bool[] buttons = state.GetButtons();
                for (int index = 0; index < buttons.Length; index++)
                {
                    if (buttons[index] == true)
                    {
                        Debug.WriteLine(index);
                        testBox.Text = gamepadIndex.ToString() + " " + index.ToString();
                    }
                }
            }
        }
    }
}
