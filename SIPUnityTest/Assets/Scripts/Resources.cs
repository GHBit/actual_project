﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityRawInput;

public class Resources : MonoBehaviour {
    public static Resources me;

    public Button[] buttons;
    public int[] numButtonPresses;

    private int? indexOfLastButtonPress = null;

    private void Awake()
    {
        if (me == null)
        {
            me = this;
        }
        else
        {
            Destroy(me);
        }

        numButtonPresses = new int[buttons.Length];
        for (int index = 0; index < buttons.Length; index++)
        {
            numButtonPresses[index] = 0;
            buttons[index].followupButtonFrequency = new int[buttons.Length];
        }
    }

    private void Start()
    {
        RawKeyInput.Start(true);
        RawKeyInput.OnKeyUp += HandleKeyUp;
        RawKeyInput.OnKeyDown += HandleKeyDown;
    }

    private void Update()
    {
        /*for (int buttonIndex = 0; buttonIndex < buttons.Length; buttonIndex++)
        {
            if (Input.GetKeyDown(buttons[buttonIndex].keyCodeP1))
            {
                numButtonPresses[buttonIndex]++;
                if (indexOfLastButtonPress != null)
                {
                    buttons[(int)indexOfLastButtonPress].followupButtonFrequency[buttonIndex]++;
                }
                indexOfLastButtonPress = buttonIndex;
            }
        }*/
    }

    private void HandleKeyUp(RawKey key)
    {

    }

    private void HandleKeyDown(RawKey key)
    {
        Debug.Log("hello");
        for (int buttonIndex = 0; buttonIndex < buttons.Length; buttonIndex++)
        {
            if (key == buttons[buttonIndex].keyCodeP1)
            {
                numButtonPresses[buttonIndex]++;
                if (indexOfLastButtonPress != null)
                {
                    buttons[(int)indexOfLastButtonPress].followupButtonFrequency[buttonIndex]++;
                }
                indexOfLastButtonPress = buttonIndex;
            }
        }
    }

    private void OnApplicationQuit()
    {
        string json = "";

        for (int index = 0; index < buttons.Length; index++)
        {
            if (numButtonPresses[index] > 0)
            {
                json += "Button " + buttons[index].buttonName + " pressed " + numButtonPresses[index].ToString() + ". Followed by \n";
                for (int secondIndex = 0; secondIndex < buttons[index].followupButtonFrequency.Length; secondIndex++)
                {
                    if (buttons[index].followupButtonFrequency[secondIndex] > 0)
                    {
                        json += buttons[secondIndex].buttonName + " * " + buttons[index].followupButtonFrequency[secondIndex].ToString() + ", ";
                    }
                }
                json += "\n";
            }
        }


        string filepath = System.DateTime.Now.ToString() + ".json";

        char[] filepathCharArray = filepath.ToCharArray();
        filepath = "Saves/";
        for (int index = 0; index < filepathCharArray.Length; index++)
        {
            if (filepathCharArray[index] == ':' || filepathCharArray[index] == '/')
            {
                filepathCharArray[index] = '_';
            }
            filepath += filepathCharArray[index];
        }

        File.WriteAllText(filepath, json);
    }

    private void SortButtonsByLength()
    {
        QuickSort(1, numButtonPresses.Length);
    }

    private void QuickSort(int lowIndex, int highIndex)
    {
        if (lowIndex < highIndex) //error prevention
        {
            int pivot = numButtonPresses[(lowIndex + highIndex) / 2];
            int leftmostUnswappedIndex = lowIndex - 1;

            for (int index = lowIndex; index < highIndex; index++)
            {
                if (numButtonPresses[index] <= pivot)
                {
                    leftmostUnswappedIndex++;
                    SwapButtons(leftmostUnswappedIndex, index);
                }
            }
            SwapButtons(leftmostUnswappedIndex + 1, highIndex);
        }
    }

    private void SwapButtons(int indexOne, int indexTwo)
    {
        Button tempButton = buttons[indexOne];
        int tempNumPresses = numButtonPresses[indexOne];

        buttons[indexOne] = buttons[indexTwo];
        numButtonPresses[indexOne] = numButtonPresses[indexTwo];

        buttons[indexTwo] = tempButton;
        numButtonPresses[indexTwo] = tempNumPresses;
    }
}
