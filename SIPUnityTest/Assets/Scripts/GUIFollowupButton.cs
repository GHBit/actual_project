﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIFollowupButton : GUIButtonTracker {
    public GUIButtonTracker parentButton;

    override protected void Awake()
    {
        base.Awake();
    }

    // Use this for initialization
    override protected void Start () {
		//Just used to prevent base Start from running
	}
	
	// Update is called once per frame
	override protected void Update () {
		for (int index = 0; index < Resources.me.buttons.Length; index++)
        {
            if (Resources.me.buttons[parentButton.buttonToRepresent].followupButtonFrequency[this.buttonToRepresent]
                < Resources.me.buttons[parentButton.buttonToRepresent].followupButtonFrequency[index])
            {
                this.buttonToRepresent = index;
            }
        }

        buttonImage.sprite = Resources.me.buttons[buttonToRepresent].pictureOfButton;
        totalNumTimesPressed.text = Resources.me.buttons[parentButton.buttonToRepresent].followupButtonFrequency[buttonToRepresent].ToString();
	}
}
