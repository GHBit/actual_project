﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityRawInput;
using UnityEngine.UI;

public class SimpleButtonPress : MonoBehaviour {
    public Text numOfInputText;
    private int numOfInput;
    public Text numOfInputPerMinuteText;
    public KeyCode inputToFind;

    public float timer;

	// Use this for initialization
	void Start () {
        RawKeyInput.Start(true);
        timer = 0;
        numOfInput = 0;
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        if (Input.GetKeyDown(inputToFind))
        {
            numOfInput++;
            numOfInputText.text = numOfInput.ToString();
        }
        numOfInputPerMinuteText.text = (numOfInput / timer * 60).ToString();
	}
}
