﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIButtonTracker : MonoBehaviour
{
    protected Image buttonImage;
    protected Text totalNumTimesPressed;

    public int buttonToRepresent;

    // Use this for initialization
    virtual protected void Awake()
    {
        buttonImage = GetComponentInChildren<Image>();
        totalNumTimesPressed = GetComponentInChildren<Text>();
    }

    // Initialization 2, Electric Boogaloo
    virtual protected void Start()
    {
        if (Resources.me.buttons.Length > buttonToRepresent)
        {
            buttonImage.sprite = Resources.me.buttons[buttonToRepresent].pictureOfButton;
        }
    }

    // Update is called once per frame
    virtual protected void Update()
    {
        if (Resources.me.buttons.Length > buttonToRepresent)
        {
            totalNumTimesPressed.text = Resources.me.numButtonPresses[buttonToRepresent].ToString();
        }
    }
}
