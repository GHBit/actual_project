﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityRawInput;

[System.Serializable]
public struct Button {
    public RawKey keyCodeP1;
    public RawKey keyCodeP2;
    public Sprite pictureOfButton;

    public string buttonName;
    public int[] followupButtonFrequency;
}
